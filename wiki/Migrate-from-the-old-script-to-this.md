## 從舊指令碼遷移至此

* 請確保你正在使用主流且不過時的發行版。

### 停止 V2Ray 服務

```
# systemctl disable v2ray.service --now
```

### 移除原有文檔

```
# rm -r /usr/bin/v2ray/
# rm /etc/systemd/system/v2ray.service
# rm /lib/systemd/system/v2ray.service
# rm /etc/init.d/v2ray
```

### 遷移配置文檔

```
# mv /etc/v2ray/ /usr/local/etc/
```

如果你不想遷移配置文檔，請在下載腳本後自行修改：

```
# vim install-release.sh

JSON_PATH='/etc/v2ray/'
```

### 修改 Log 權限

#### nobody:nogroup

```
$ id nobody

uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
```

```
# chown -R nobody:nogroup /var/log/v2ray/
```

#### nobody:nobody

```
$ id nobody

uid=65534(nobody) gid=65534(nobody) groups=65534(nobody)
```

```
# chown -R nobody:nobody /var/log/v2ray/
```

### 安裝 cURL

#### Debian

```
# apt update
# apt install curl
```

#### CentOS / Fedora

```
# dnf makecache
# dnf install curl
```

#### openSUSE

```
# zypper refresh
# zypper install curl
```

### 下載此指令碼

```
$ curl -O https://raw.githubusercontent.com/v2fly/fhs-install-v2ray/master/install-release.sh
```

### 執行此指令碼

```
# bash install-release.sh
```

### 注意事項

1. 你可能會對多文檔配置結構感到困惑，如果你仍希望使用原有的 `config.json`，請移除這些對你而言多餘的文檔。
2. 如果你不打算通過 systemd 來管理 V2Ray，請自行處理 `V2RAY_LOCATION_ASSET`。
