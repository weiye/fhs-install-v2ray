## 将 .dat 文档由 lib 目录移动到 share 目录

目前已无需移动。

---

如果你需要将 `/usr/local/lib/v2ray/` 移动到 `/usr/local/share/` 下。

### 修改脚本

```
# vim install-release.sh

DAT_PATH='/usr/local/share/v2ray/'
```

### 移动目录

```
# mv /usr/local/lib/v2ray/ /usr/local/share/
```

### 修改 systemd 单元文件

```
# systemctl edit v2ray.service

[Service]
Environment=V2RAY_LOCATION_ASSET=/usr/local/share/v2ray/
```
