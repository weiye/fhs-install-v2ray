## 使用 VLESS 协议

### 更细致的权限管理

```
# useradd -s /usr/sbin/nologin v2ray
```

#### 修改单元文件

```
# systemctl edit v2ray.service

[Service]
User=v2ray
```

### 通过 Certbot 管理证书

这里假定你已经通过 Certbot 获取了证书文档。

1. `/etc/letsencrypt/live/example.com/fullchain.pem`。
2. `/etc/letsencrypt/live/example.com/privkey.pem`。

如果你使用 Debian 发行版，在安装 Certbot 后，`renew` 是会定期运行的。

创建 V2Ray 专用的证书文档目录：

```
# install -d -o v2ray -g v2ray /etc/ssl/v2ray/
```

将证书以指定权限部署到指定路径：

```
# install -m 644 -o v2ray -g v2ray /etc/letsencrypt/live/example.com/fullchain.pem -t /etc/ssl/v2ray/
# install -m 600 -o v2ray -g v2ray /etc/letsencrypt/live/example.com/privkey.pem -t /etc/ssl/v2ray/
```

为在后续 `renew` 重新部署证书后，自动重新部署证书到指定路径，并重启 V2Ray 服务，需要一个脚本，以下是参照内容：

```bash
# vim /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh

#!/bin/bash

V2RAY_DOMAIN='example.com'

if [[ "$RENEWED_LINEAGE" == "/etc/letsencrypt/live/$V2RAY_DOMAIN" ]]; then
    install -m 644 -o v2ray -g v2ray "/etc/letsencrypt/live/$V2RAY_DOMAIN/fullchain.pem" -t /etc/ssl/v2ray/
    install -m 600 -o v2ray -g v2ray "/etc/letsencrypt/live/$V2RAY_DOMAIN/privkey.pem" -t /etc/ssl/v2ray/

    sleep "$((RANDOM % 2048))"
    systemctl restart v2ray.service
fi
```

#### 给予脚本可运行权限

```
# chmod +x /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh
```

`renew` 重新部署证书后，会自动运行 `deploy/` 下具有可运行权限的脚本。

### 修改 V2Ray 配置文档

```json
"certificates": [
    {
        "certificateFile": "/etc/ssl/v2ray/fullchain.pem",
        "keyFile": "/etc/ssl/v2ray/privkey.pem"
    }
]
```
