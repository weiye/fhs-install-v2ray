如果你有在运行脚本时，不安装或更新 `geoip.dat` 和 `geosite.dat` 的需求。

请运行：

```
# touch /usr/local/share/v2ray/.undat
```
