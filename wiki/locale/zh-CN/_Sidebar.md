[**繁體中文**](https://github.com/v2fly/fhs-install-v2ray/wiki/Home)

[**简体中文**](https://github.com/v2fly/fhs-install-v2ray/wiki/Home-zh-Hans-CN)

* * *

* **[首页](https://github.com/v2fly/fhs-install-v2ray/wiki/Home-zh-Hans-CN)**

## 迁移

* **[从旧脚本迁移至此](https://github.com/v2fly/fhs-install-v2ray/wiki/Migrate-from-the-old-script-to-this-zh-Hans-CN)**&#8203;**（从这里开始）**
* **[将 .dat 文档由 lib 目录移动到 share 目录](https://github.com/v2fly/fhs-install-v2ray/wiki/Move-.dat-files-from-lib-directory-to-share-directory-zh-Hans-CN)**

## 常见问题

* **[使用证书时权限不足](https://github.com/v2fly/fhs-install-v2ray/wiki/Insufficient-permissions-when-using-certificates-zh-Hans-CN)**

## 高级

* **[不安装或更新 geoip.dat 和 geosite.dat](https://github.com/v2fly/fhs-install-v2ray/wiki/Do-not-install-or-update-geoip.dat-and-geosite.dat-zh-Hans-CN)**
* **[使用 VLESS 协议](https://github.com/v2fly/fhs-install-v2ray/wiki/To-use-the-VLESS-protocol-zh-Hans-CN)**
