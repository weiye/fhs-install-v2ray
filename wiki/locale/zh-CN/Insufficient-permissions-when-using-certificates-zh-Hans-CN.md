## 使用证书时权限不足

### 方案一（不推荐）

假定证书文档分别为：

1. `/srv/http/example.com.pem`。
2. `/srv/http/example.com.key`。

据此：

1. `/srv/http/` 的默认权限一般为 755；
2. `/srv/http/example.com.pem` 的默认权限一般为 644。
3. `/srv/http/example.com.key` 的默认权限一般为 600；

将 `/srv/http/example.com.key` 修改为 644 即可：

```
# chmod 644 /srv/http/example.com.key
```

### 方案二

假定证书文档分别为：

1. `/srv/http/example.com.pem`。
2. `/srv/http/example.com.key`。

```
# id nobody
```

* 输出的结果可能是：

```
uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
```

相应的，只需要执行：

```
# chown -R nobody:nogroup /srv/http/
```

* 输出的结果也可能是：

```
uid=65534(nobody) gid=65534(nobody) groups=65534(nobody)
```

相应的，只需要执行：

```
# chown -R nobody:nobody /srv/http/
```

### 方案三（Certbot）

这里假定你已经通过 Certbot 获取了证书文档。

1. `/etc/letsencrypt/live/example.com/fullchain.pem`。
2. `/etc/letsencrypt/live/example.com/privkey.pem`。

如果你使用 Debian 发行版，在安装 Cerbot 后，`renew` 是会定期运行的。

创建 V2Ray 专用的证书文档目录：

```
# install -d -o nobody -g nogroup /etc/ssl/v2ray/
```

将证书文档以指定权限部署到指定路径：

```
# install -m 644 -o nobody -g nogroup /etc/letsencrypt/live/example.com/fullchain.pem -t /etc/ssl/v2ray/
# install -m 600 -o nobody -g nogroup /etc/letsencrypt/live/example.com/privkey.pem -t /etc/ssl/v2ray/
```

为在后续 `renew` 中自动重新部署，需要一个脚本，以下是参照内容：

```bash
# vim /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh

#!/bin/bash

V2RAY_DOMAIN='example.com'

if [[ "$RENEWED_LINEAGE" == "/etc/letsencrypt/live/$V2RAY_DOMAIN" ]]; then
    install -m 644 -o nobody -g nogroup "/etc/letsencrypt/live/$V2RAY_DOMAIN/fullchain.pem" -t /etc/ssl/v2ray/
    install -m 600 -o nobody -g nogroup "/etc/letsencrypt/live/$V2RAY_DOMAIN/privkey.pem" -t /etc/ssl/v2ray/

    sleep "$((RANDOM % 2048))"
    systemctl restart v2ray.service
fi
```

#### 给予脚本可运行权限

```
# chmod +x /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh
```

`renew` 重新部署证书后，会自动运行 `deploy/` 下具有可运行权限的脚本。
