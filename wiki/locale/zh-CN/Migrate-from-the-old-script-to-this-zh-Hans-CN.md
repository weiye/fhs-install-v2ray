## 从旧脚本迁移至此

* 请确保你正在使用主流且不过时的发行版。

### 停止 V2Ray 服务

```
# systemctl disable v2ray.service --now
```

### 移除原有文档

```
# rm -r /usr/bin/v2ray/
# rm /etc/systemd/system/v2ray.service
# rm /lib/systemd/system/v2ray.service
# rm /etc/init.d/v2ray
```

### 迁移配置文档

```
# mv /etc/v2ray/ /usr/local/etc/
```

如果你不想迁移配置文档，请在下载脚本后自行修改：

```
# vim install-release.sh

JSON_PATH='/etc/v2ray/'
```

### 修改 Log 权限

#### nobody:nogroup

```
$ id nobody

uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
```

```
# chown -R nobody:nogroup /var/log/v2ray/
```

#### nobody:nobody

```
$ id nobody

uid=65534(nobody) gid=65534(nobody) groups=65534(nobody)
```

```
# chown -R nobody:nobody /var/log/v2ray/
```

### 安装 cURL

#### Debian

```
# apt update
# apt install curl
```

#### CentOS / Fedora

```
# dnf makecache
# dnf install curl
```

#### openSUSE

```
# zypper refresh
# zypper install curl
```

### 下载此脚本

```
$ curl -O https://raw.githubusercontent.com/v2fly/fhs-install-v2ray/master/install-release.sh
```

### 运行此脚本

```
# bash install-release.sh
```

### 注意事项

1. 你可能会对多文档配置结构感到困惑，如果你仍希望使用原有的 `config.json`，请移除这些对你而言多余的文档。
2. 如果你不打算通过 systemd 来管理 V2Ray，请自行处理 `V2RAY_LOCATION_ASSET`。
