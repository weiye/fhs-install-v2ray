## 使用 VLESS 協議

### 更細緻的權限管理

```
# useradd -s /usr/sbin/nologin v2ray
```

#### 修改單元檔案

```
# systemctl edit v2ray.service

[Service]
User=v2ray
```

### 通過 Certbot 管理證書

這裡假定你已經通過 Certbot 獲取了證書文檔。

1. `/etc/letsencrypt/live/example.com/fullchain.pem`。
2. `/etc/letsencrypt/live/example.com/privkey.pem`。

如果你使用 Debian 發行版，在安裝 Certbot 後，`renew` 是會定期執行的。

建立 V2Ray 專用的證書文檔目錄：

```
# install -d -o v2ray -g v2ray /etc/ssl/v2ray/
```

將證書以指定權限部署到指定路徑：

```
# install -m 644 -o v2ray -g v2ray /etc/letsencrypt/live/example.com/fullchain.pem -t /etc/ssl/v2ray/
# install -m 600 -o v2ray -g v2ray /etc/letsencrypt/live/example.com/privkey.pem -t /etc/ssl/v2ray/
```

為在後續 `renew` 重新部署證書後，自動重新部署證書到指定路徑，並重啟 V2Ray 服務，需要一個腳本，以下是參照內容：

```bash
# vim /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh

#!/bin/bash

V2RAY_DOMAIN='example.com'

if [[ "$RENEWED_LINEAGE" == "/etc/letsencrypt/live/$V2RAY_DOMAIN" ]]; then
    install -m 644 -o v2ray -g v2ray "/etc/letsencrypt/live/$V2RAY_DOMAIN/fullchain.pem" -t /etc/ssl/v2ray/
    install -m 600 -o v2ray -g v2ray "/etc/letsencrypt/live/$V2RAY_DOMAIN/privkey.pem" -t /etc/ssl/v2ray/

    sleep "$((RANDOM % 2048))"
    systemctl restart v2ray.service
fi
```

#### 給予腳本可執行權限

```
# chmod +x /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh
```

`renew` 重新部署證書後，會自動執行 `deploy/` 下具有可執行權限的腳本。

### 修改 V2Ray 配置文檔

```json
"certificates": [
    {
        "certificateFile": "/etc/ssl/v2ray/fullchain.pem",
        "keyFile": "/etc/ssl/v2ray/privkey.pem"
    }
]
```
