## 使用證書時權限不足

### 方案一（不推薦）

假定證書文檔分別為：

1. `/srv/http/example.com.pem`。
2. `/srv/http/example.com.key`。

據此：

1. `/srv/http/` 的默認權限一般為 755；
2. `/srv/http/example.com.pem` 的默認權限一般為 644。
3. `/srv/http/example.com.key` 的默認權限一般為 600；

將 `/srv/http/example.com.key` 修改為 644 即可：

```
# chmod 644 /srv/http/example.com.key
```

### 方案二

假定證書文檔分別為：

1. `/srv/http/example.com.pem`。
2. `/srv/http/example.com.key`。

```
# id nobody
```

* 輸出的结果可能是：

```
uid=65534(nobody) gid=65534(nogroup) groups=65534(nogroup)
```

相应的，只需要执行：

```
# chown -R nobody:nogroup /srv/http/
```

* 輸出的结果也可能是：

```
uid=65534(nobody) gid=65534(nobody) groups=65534(nobody)
```

相应的，只需要执行：

```
# chown -R nobody:nobody /srv/http/
```

### 方案三（Certbot）

這裡假定你已經通過 Certbot 獲取了證書文檔。

1. `/etc/letsencrypt/live/example.com/fullchain.pem`。
2. `/etc/letsencrypt/live/example.com/privkey.pem`。

如果你使用 Debian 發行版，在安裝 Cerbot 後，`renew` 是會定期執行的。

建立 V2Ray 專用的證書文檔目錄：

```
# install -d -o nobody -g nogroup /etc/ssl/v2ray/
```

將證書文檔以指定權限部署到指定路徑：

```
# install -m 644 -o nobody -g nogroup /etc/letsencrypt/live/example.com/fullchain.pem -t /etc/ssl/v2ray/
# install -m 600 -o nobody -g nogroup /etc/letsencrypt/live/example.com/privkey.pem -t /etc/ssl/v2ray/
```

為在後續 `renew` 中自動重新部署，需要一個腳本，以下是參照內容：

```bash
# vim /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh

#!/bin/bash

V2RAY_DOMAIN='example.com'

if [[ "$RENEWED_LINEAGE" == "/etc/letsencrypt/live/$V2RAY_DOMAIN" ]]; then
    install -m 644 -o nobody -g nogroup "/etc/letsencrypt/live/$V2RAY_DOMAIN/fullchain.pem" -t /etc/ssl/v2ray/
    install -m 600 -o nobody -g nogroup "/etc/letsencrypt/live/$V2RAY_DOMAIN/privkey.pem" -t /etc/ssl/v2ray/

    sleep "$((RANDOM % 2048))"
    systemctl restart v2ray.service
fi
```

#### 給予腳本可執行權限

```
# chmod +x /etc/letsencrypt/renewal-hooks/deploy/v2ray.sh
```

`renew` 重新部署證書後，會自動執行 `deploy/` 下具有可執行權限的腳本。
