[**简体中文**](https://github.com/v2fly/fhs-install-v2ray/wiki/Home-zh-Hans-CN)

[**繁體中文**](https://github.com/v2fly/fhs-install-v2ray/wiki/Home)

* * *

* **[首頁](https://github.com/v2fly/fhs-install-v2ray/wiki/Home)**

## 遷移

* **[從舊指令碼遷移至此](https://github.com/v2fly/fhs-install-v2ray/wiki/Migrate-from-the-old-script-to-this)**&#8203;**（從這裡開始）**
* **[將 .dat 文件由 lib 目錄移動到 share 目錄](https://github.com/v2fly/fhs-install-v2ray/wiki/Move-.dat-files-from-lib-directory-to-share-directory)**

## 常見問題

* **[使用證書時許可權不足](https://github.com/v2fly/fhs-install-v2ray/wiki/Insufficient-permissions-when-using-certificates)**

## 高階

* **[不安裝或更新 geoip.dat 和 geosite.dat](https://github.com/v2fly/fhs-install-v2ray/wiki/Do-not-install-or-update-geoip.dat-and-geosite.dat)**
* **[使用 VLESS 協議](https://github.com/v2fly/fhs-install-v2ray/wiki/To-use-the-VLESS-protocol)**
