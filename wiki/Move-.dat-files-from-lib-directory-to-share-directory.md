## 將 .dat 文檔由 lib 目錄移動到 share 目錄

目前已無需移動。

---

如果你需要將 `/usr/local/lib/v2ray/` 移動到 `/usr/local/share/` 下。

### 修改腳本

```
# vim install-release.sh

DAT_PATH='/usr/local/share/v2ray/'
```

### 移動目錄

```
# mv /usr/local/lib/v2ray/ /usr/local/share/
```

### 修改單元檔案

```
# systemctl edit v2ray.service

[Service]
Environment=V2RAY_LOCATION_ASSET=/usr/local/share/v2ray/
```
