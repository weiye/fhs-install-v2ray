如果你有在運行腳本時，不安裝或更新 `geoip.dat` 和 `geosite.dat` 的需求。

請執行：

```
# touch /usr/local/share/v2ray/.undat
```
